<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/app/', function () {
    ob_start();
    require dirname(__DIR__, 1).'/ai/client/247_imag/index.php';
    echo ob_get_clean();
})->middleware(['auth'])->name('app');

Route::post('/', function () {
    ob_start();
    require dirname(__DIR__, 1).'/ai/client/247_imag/index.php';
    echo ob_get_clean();
})->middleware(['auth'])->name('proxy');

require __DIR__.'/auth.php';
