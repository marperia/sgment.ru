<?php

namespace system;

use System\Actions\Index;
use System\Actions\ProductForm;
use System\Actions\ProductTable;
use System\Actions\Selectors;
use System\Actions\TableSettings;
use System\Classes\Route;

Route::add('', 'index', Index::class);
Route::add('product_table', 'index', ProductTable::class);
Route::add('update_active', 'updateActive', ProductTable::class);

Route::add('table_settings', 'form', TableSettings::class);
Route::add('product_form', 'form', ProductForm::class);
Route::add('product_form_store', 'store', ProductForm::class);
Route::add('category_selector', 'category', Selectors::class);
Route::add('brand_selector', 'brand', Selectors::class);
