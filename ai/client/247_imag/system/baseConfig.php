<?php
return [
    /**
     * URL адрес скрипта
     */
    'host' => 'http://localhost',

    /**
     * Кэширование шаблонов
     */
    'cached_templates' => true,

    /**
     * Время жизни кэша данных
     */
    'data_cache_time' => '86400',

    /**
     * Список БД
     */
    'databases' => [
        /**
         * БД по умолчанию
         */
        'default' => [
            'host'     => '',
            'port'     => '3306',
            'username' => '',
            'password' => '',
            'database' => '',
        ],
    ],
];
