<?php

namespace System\Classes;

class JsonResponse
{
    public array $data = [];

    protected function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public static function make($data)
    : self
    {
        return new self($data);
    }

    public function display()
    {
        header('Content-type:application/json;charset=utf-8');

        echo json_encode($this->data);
    }
}
