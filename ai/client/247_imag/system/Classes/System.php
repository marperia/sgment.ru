<?php

namespace System\Classes;

use Phpfastcache\CacheManager;
use Phpfastcache\Config\ConfigurationOption;
use Phpfastcache\Exceptions\PhpfastcacheInvalidConfigurationException;
use ReflectionException;
use System\Exceptions\NotFoundException;
use Throwable;
use WebPlace\Logger;

class System
{

    public Request $request;
    public Display $display;
    public Config $config;

    static ?System $instance = null;

    protected function __construct()
    {
    }

    private function init()
    {
        $this->config = new Config();
        $this->request = new Request($_GET, $_POST, $_COOKIE, $_FILES, $_SERVER);
        $this->display = new Display();
    }

     public function run()
    {
        try {
            $this->initCache();

            $content = Route::resolve($this);
            $this->display->setContent($content)->printHtml();
        }
        catch (Throwable $e){
            if ($e instanceof NotFoundException){
                $this->display->printHttpError(404, 'Страница не существует. Подробнее:' . $e->getMessage());
            }
            else {
                Logger::writeExceptionLog('Main exception', $e);

                $this->display->printHttpError(500, $e->getMessage());
            }
        }
    }

    public static function getInstance()
    : System
    {
        if (empty(self::$instance)){
            self::$instance = new self();
            self::$instance->init();
        }

        return self::$instance;
    }

    /**
     * @throws ReflectionException
     * @throws PhpfastcacheInvalidConfigurationException
     */
    protected function initCache()
    {
        CacheManager::setDefaultConfig(new ConfigurationOption([
            'path' => CACHE_DIR,
        ]));
    }
}


