<?php

namespace System\Classes;

use Exception;
use Krugozor\Database\Mysql;

class DB
{
    /**
     * @var Mysql[]
     */
    protected static array $connections;

    public static function connect(string $conName = 'default')
    {
        if (empty(static::$connections[$conName])){
            $settings = self::getSettings($conName);

            static::$connections[$conName] = Mysql::create($settings['host'], $settings['username'], $settings['password'], $settings['port'])
                // Выбор базы данных
                ->setDatabaseName($settings['database'])
                // Выбор кодировки
                ->setCharset('utf8');
        }

        return static::$connections[$conName];
    }

    /**
     * @throws Exception
     */
    public static function getSettings(string $conName = 'default')
    {
        $config = System::getInstance()->config->all();

        if (array_key_exists($conName, $config['databases'])){
            return array_merge([
                'host'     => '',
                'port'     => '3306',
                'username' => '',
                'password' => '',
                'database' => '',
            ], $config['databases'][$conName]);
        }

        throw new Exception("Настроек соединения [$conName] не найдено");
    }
}
