<?php

namespace System\Classes;

class AbstractAction
{
    protected System $system;
    protected Request $request;

    public function __construct(System $system)
    {
        $this->system = $system;
        $this->request = $this->system->request;
    }
}
