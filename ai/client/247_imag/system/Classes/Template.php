<?php

namespace System\Classes;

class Template
{
    public string $name;
    public string $file;
    public array $params = [];

    protected function __construct($templateName, array $params = [])
    {
        $this->name = $templateName;
        $this->file = str_replace('.', DIRECTORY_SEPARATOR, $templateName) . '.twig';
        $this->params = $params;
    }

    public static function make($templateName, array $params = [])
    : Template
    {
        return new self($templateName, $params);
    }
}
