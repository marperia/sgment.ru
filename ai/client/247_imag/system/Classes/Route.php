<?php

namespace System\Classes;

use ReflectionClass;
use ReflectionException;
use System\Exceptions\NotFoundException;

class Route
{
    static array $routes = [];

    public static function add(string $url, string $method, string $actionClass)
    {
        self::$routes[$url] = [$actionClass, $method];
    }

    /**
     * @throws ReflectionException
     * @throws NotFoundException
     */
    public static function resolve(System $system)
    {
        if (!empty(self::$routes)){
            $action = $system->request->getAction();

            if (array_key_exists($action, self::$routes)){
                [$class, $method] = self::$routes[$action];

                try {
                    $reflector = new ReflectionClass($class);
                }
                catch (ReflectionException $e){
                    throw new NotFoundException("Указанный класс [$class] не существует.", 0, $e);
                }

                if (!$reflector->hasMethod($method)){
                    throw new NotFoundException("Указанный метод [$method] не существует.");
                }

                $object = $reflector->newInstanceArgs([
                    $system,
                ]);

                return $reflector->getMethod($method)->invokeArgs($object, []);
            }
            else {
                throw new NotFoundException("Маршрут [$action] не найден");
            }
        }

        return null;
    }
}
