<?php

namespace System\Classes;

use RecursiveArrayIterator;
use RecursiveIteratorIterator;

class Config
{
    private array $optionsData;
    private array $options;

    public function __construct()
    {
        $this->initConfig();
    }

    public function option($name, $default = null)
    {
        if (array_key_exists($name, $this->options)){
            return $this->options[$name];
        }

        return $default;
    }

    public function all()
    : array
    {
        return $this->optionsData;
    }

    private function initConfig()
    {
        $base = require SYSTEM_DIR . 'baseConfig.php';
        $config = require SYSTEM_DIR . 'config.php';

        $this->optionsData = array_merge($base, $config);

        $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($this->optionsData));

        foreach ($iterator as $item) {
            $keys = array();

            foreach (range(0, $iterator->getDepth()) as $depth) {
                $keys[] = $iterator->getSubIterator($depth)->key();
            }

            $this->options[join('.', $keys)] = $item;
        }
    }
}
