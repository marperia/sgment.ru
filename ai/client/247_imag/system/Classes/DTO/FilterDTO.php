<?php

namespace System\Classes\DTO;

use System\Classes\Request;

class FilterDTO
{
    public string $category = '';
    public string $manufacturer = '';
    public string $search = '';
    public int $lastUpdateFilter = 0;
    public int $dateAddedFilter = 0;
    public int $dateModifiedFilter = 0;

    protected function __construct()
    {
    }

    public static function makeFromRequest (Request $request)
    : FilterDTO
    {
        $self = new static();

        $self->category = $request->get('category_filter', '');
        $self->manufacturer = $request->get('brand_filter', '');
        $self->lastUpdateFilter = $request->get('last_upd_filter', 0);
        $self->dateAddedFilter = $request->get('date_added_filter', 0);
        $self->dateModifiedFilter = $request->get('date_modified_filter', 0);

        $search = $request->get('search', []);

        if (is_array($search) && array_key_exists('value', $search)){
            $self->search = $search['value'];
        }
        else if (is_string($search)){
            $self->search = $search;
        }

        return $self;
    }
}
