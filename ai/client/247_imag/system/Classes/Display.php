<?php

namespace System\Classes;

use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class Display
{
    /**
     * @var mixed
     */
    protected $content;

    private ?FilesystemLoader $twigLoader = null;
    private ?Environment $twig = null;

    /**
     * @param $content
     *
     * @return $this
     */
    public function setContent($content)
    : Display
    {
        $this->content = $content;

        return $this;
    }

    protected function getTwig()
    : Environment
    {
        if (is_null($this->twig)){
            $this->twig = new Environment($this->getTwigLoader(), [
                'cache' => CACHE_DIR . 'views',
                'auto_reload' => !System::getInstance()->config->option('cached_templates', true),
            ]);

            $this->twig->addGlobal('APP_HOST', System::getInstance()->config->option('host'));
        }

        return $this->twig;
    }

    protected function getTwigLoader()
    : FilesystemLoader
    {
        if (is_null($this->twigLoader)){
            $this->twigLoader = new FilesystemLoader(TEMPLATES_DIR);
        }

        return $this->twigLoader;
    }

    protected function processTemplate(Template $template)
    {
        if (!file_exists(TEMPLATES_DIR . $template->file)){
            $this->printInternalError('Ошибка загрузки шаблона', "Шаблон {$template->file} не найден.");
        }
        else {
            $this->getTwig()->display($template->file, $template->params);
        }
    }

    public function printHtml()
    {
        if ($this->content instanceof Template){
            $this->processTemplate($this->content);
        }
        else if ($this->content instanceof JsonResponse){
            $this->content->display();
        }
        else if (is_array($this->content) || is_object($this->content)){
            echo '<pre>';
            print_r($this->content);
            echo '</pre>';
        }
        else {
            echo $this->content;
        }
    }

    public function printHttpError(int $code, $message = '')
    {
        http_response_code($code);

        $this->twigPrint('system' . DIRECTORY_SEPARATOR . 'http_error.twig', ['errorCode' => $code, 'message' => $message]);
    }

    public function printInternalError(string $title, string $message)
    {
        $this->twigPrint('system' . DIRECTORY_SEPARATOR . 'internal_error.twig', compact('title', 'message'));
    }

    public function twigPrint(string $tplFile, array $attribute = [])
    {
        $this->getTwig()->display($tplFile, $attribute);
    }
}
