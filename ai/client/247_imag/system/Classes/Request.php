<?php

namespace System\Classes;

class Request
{
    const ALL_REQUEST = 0;
    const GET_REQUEST = 1;
    const POST_REQUEST = 2;
    const FILE_REQUEST = 3;

    public array $request;

    public array $query;

    public array $server;

    public array $files;

    public array $cookies;

    public array $headers;

    protected ?string $actionName = null;

    public function __construct(array $query = [], array $request = [], array $cookies = [], array $files = [], array $server = [])
    {
        $this->initialize($query, $request, $cookies, $files, $server);
    }

    public function initialize(array $query = [], array $request = [], array $cookies = [], array $files = [], array $server = [])
    {
        $this->request = $request;
        $this->query = $query;
        $this->cookies = $cookies;
        $this->files = $files;
        $this->server = $server;
        //$this->headers = $this->server->getHeaders();
    }

    public function all()
    : array
    {
        return $this->request + $this->query;
    }

    public function get(string $param, $default = null, int $requestType = self::ALL_REQUEST)
    {
        switch ($requestType){
            case self::ALL_REQUEST:
                $input = $this->all();

                if (array_key_exists($param, $input)){
                    return $input[$param];
                }

                return $default;

            case self::GET_REQUEST:
                if (array_key_exists($param, $this->query)){
                    return $this->query[$param];
                }

                return $default;

            case self::POST_REQUEST:
                if (array_key_exists($param, $this->request)){
                    return $this->request[$param];
                }

                return $default;

            default:
                return $default;
                //throw new \ErrorException('Неизвестный тип запроса: ' . $requestType);
        }
    }

    public function getAction()
    : ?string
    {
        if (is_null($this->actionName)){
            $this->actionName = $this->get('action', null, self::GET_REQUEST);

            if (!is_null($this->actionName)){
                $this->actionName = strtolower(filter_var($this->actionName, FILTER_SANITIZE_SPECIAL_CHARS));
            }
        }

        return $this->actionName;
    }

    public function __get(string $param)
    {
        return $this->get($param);
    }
}
