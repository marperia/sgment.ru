<?php

namespace System\Actions;

use ArrayObject;
use Phpfastcache\CacheManager;
use System\Classes\AbstractAction;
use System\Classes\DB;
use System\Classes\DTO\FilterDTO;
use System\Classes\JsonResponse;
use System\Classes\System;

class Selectors extends AbstractAction
{
    const PAGINATION = 100;

    private ProductTable $productTable;
    private bool $filtered;

    public function __construct(System $system)
    {
        parent::__construct($system);

        $this->filtered = $this->request->get('filtered') == 'true';

        $this->productTable = new ProductTable($system);
    }

    public function category()
    : JsonResponse
    {
        $data = $this->getCategories();

        return $this->buildResponse($data);
    }

    public function brand()
    : JsonResponse
    {
        $data = $this->getBrands();

        return $this->buildResponse($data);
    }

    public function getCategories()
    : array
    {
        return $this->fromCache('product_categories', function () {
            if ($this->filtered){
                $filterDTO = FilterDTO::makeFromRequest($this->request);
                $filterDTO->category = '';

                $where = $this->productTable->makeWhere($filterDTO, true);
            }
            else {
                $where = '';
            }

            $query = DB::connect()->query(
                "SELECT DISTINCT category
                FROM kvp_shop_product
                {$where}
                GROUP BY category
                ORDER BY category");

            $obj = new ArrayObject();

            while ($row = $query->fetchAssoc()){
                $categoryPath = explode('|', $row['category']);

                for ($i = 1; $i <= count($categoryPath); $i++){
                    $category = implode('|', array_slice($categoryPath, 0, $i));

                    if (!$obj->offsetExists(strtolower($category))){
                        $obj->offsetSet(strtolower($category), $category);

                        yield $category;
                    }
                }
            }
        }, $this->filtered);
    }

    private function getBrands()
    : array
    {
        return $this->fromCache('product_brands', function () {
            if ($this->filtered){
                $filterDTO = FilterDTO::makeFromRequest($this->request);
                $filterDTO->manufacturer = '';

                $where = $this->productTable->makeWhere($filterDTO, true);
            }
            else {
                $where = '';
            }

            $query = DB::connect()->query(
                "SELECT DISTINCT manufacturer
                FROM kvp_shop_product
                {$where}
                GROUP BY manufacturer
                ORDER BY manufacturer");

            while ($row = $query->fetchAssoc()){
                yield $row['manufacturer'];
            }
        }, $this->filtered);
    }

    private function buildResponse($data)
    : JsonResponse
    {
        $page = $this->request->get('page', 0);
        $search = $this->request->get('term');

        if (!empty($search)){
            $data['rows'] = array_filter($data['rows'], function ($item) use ($search) {
                return mb_stripos($item['text'], $search) !== false;
            });

            $data['count'] = count($data['rows']);
        }

        $pagesCount = ceil($data['count'] / self::PAGINATION);

        if ($page < 1){
            $page = 1;
        }
        else if ($page > $pagesCount){
            $page = $pagesCount;
        }

        $start = $page * self::PAGINATION - self::PAGINATION;
        $end = $start + self::PAGINATION;

        $results = array_slice($data['rows'], $start, $end);

        return JsonResponse::make([
            'results'    => array_values($results),
            'count'    => $data['count'],
            'pagination' => [
                'more' => $page < $pagesCount,
            ],
        ]);
    }

    private function fromCache($name, $callback, $isSearch = false)
    : array
    {
        function getData($callback)
        : array
        {
            $iterator = $callback();
            $items = [];
            foreach ($iterator as $item){
                $items[$item] = [
                    'id'   => $item,
                    'text' => $item,
                ];
            }

            return [
                'rows'  => $items,
                'count' => count($items),
            ];
        }

        if ($isSearch){
            return getData($callback);
        }

        $cacheDrive = CacheManager::getInstance('files');
        $cache = $cacheDrive->getItem($name);

        if (!$cache->isHit()){
            $data = getData($callback);

            $cache->set($data)->expiresAfter($this->system->config->option('data_cache_time'));
            $cacheDrive->save($cache);
        }
        else {
            $data = $cache->get();
        }

        return $data;
    }
}
