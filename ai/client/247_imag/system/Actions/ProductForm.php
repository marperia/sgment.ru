<?php

namespace System\Actions;

use System\Classes\AbstractAction;
use System\Classes\DB;
use System\Classes\Template;

class ProductForm extends AbstractAction
{
    protected array $fields = [
        'category'       => 'text',
        'name'           => 550,
        'model'          => 255,
        'sku'            => 255,
        'url'            => 255,
        'location'       => 255,
        'ean'            => 255,
        'jan'            => 255,
        'mpn'            => 255,
        'upc'            => 255,
        'discount_price' => 'float',
        'price'          => 'float',
        'stock_status'   => 255,
        'manufacturer'   => 255,
        'description'    => 'text',
        'attributes'     => 'text',
        'images'         => 'text',
        'quantity'       => 'int',
        'status'         => 'bool',
    ];

    public function form()
    : Template
    {
        $productId = $this->request->get('id');
        $dataFields = [];
        $images = [];

        if ($productId > 0){
            $query = DB::connect()->query('SELECT ' . implode(',', array_keys($this->fields)) . ' FROM ?f WHERE id = ?i', 'kvp_shop_product', $productId);
            $dataFields = $query->fetchAssoc();

            foreach ($dataFields as $key => &$field){
                $field = [
                    'value'  => $field,
                    'filter' => $this->fields[$key],
                ];
            }
            unset($data);

            if (!empty($dataFields['images'])){
                $images = explode(',', $dataFields['images']['value']);
            }
        }
        else {
            foreach ($this->fields as $key => $filter){
                $dataFields[$key] = [
                    'value'  => '',
                    'filter' => $filter,
                ];
            }
        }

        return Template::make('pages.product_form', compact('productId', 'dataFields', 'images'));
    }

    public function store()
    {
        $fields = $this->request->all();
        $id = $this->request->get('id', 0);

        if (empty($fields['url'])){
            return 'Введите Url';
        }

        $toProcess = [];

        if ($id > 0){
            $query = DB::connect()->query('SELECT ' . implode(',', array_keys($this->fields)) . ' FROM kvp_shop_product WHERE id = ?i', $this->request->get('id'));
            $toProcess = $query->fetchAssoc();

            $ifEdit = 'AND `id`<>?i';
        }
        else {
            $ifEdit = '';
        }

        $existUrl = DB::connect()->query("SELECT 1 FROM kvp_shop_product WHERE `url`='?s' $ifEdit LIMIT 1" , $toProcess['url'], $id)
            ->getOne();

        if ($existUrl){
            return 'Такой Url уже существует';
        }

        if (!empty($fields['images']) && is_array($fields['images'])){
            $fields['images'] = array_diff($fields['images'], ['']);
            $fields['images'] = implode(',', $fields['images']);
        }

        foreach ($this->fields as $key => $filter){
            if (isset($fields[$key])){
                if ($filter == 'int'){
                    $toProcess[$key] = (int) $fields[$key];
                }
                else if ($filter == 'float'){
                    $toProcess[$key] = (float) filter_var($fields[$key], FILTER_VALIDATE_FLOAT);
                }
                else if ($filter == 'bool'){
                    $toProcess[$key] = (int) $fields[$key] == 1;
                }
                else if (is_integer($filter)){
                    $toProcess[$key] = substr($fields[$key], 0, $filter);
                }
                else {
                    $toProcess[$key] = (string) $fields[$key];
                }
            }
            else if (!isset($toProcess[$key])){
                $toProcess[$key] = null;
            }
        }

        if ($id > 0){
            DB::connect()->query('UPDATE `kvp_shop_product` SET ?As,`date_modified` = NOW() WHERE `id` = ?i', $toProcess, $id);
        }
        else {
            DB::connect()->query('INSERT INTO `kvp_shop_product` SET ?As, `date_added` = NOW(), `date_modified` = NOW(), `date_parsing` = NOW()', $toProcess);
        }

        return 'OK';
    }

    public function updateActive()
    {
        return DB::connect()->query(
            "UPDATE ?f SET status=?i WHERE id IN (?ai)", 'kvp_shop_product', $this->request->get('action', 0), $this->request->get('items', []));
    }
}
