<?php

namespace System\Actions;

use System\Classes\AbstractAction;
use System\Classes\Template;

class Index extends AbstractAction
{
    public function index()
    : Template
    {
        return Template::make('pages.index');
    }
}
