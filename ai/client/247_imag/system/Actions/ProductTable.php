<?php

namespace System\Actions;

use System\Classes\DTO\FilterDTO;
use Ozdemir\Datatables\Datatables;
use Ozdemir\Datatables\DB\MySQL;
use System\Classes\AbstractAction;
use System\Classes\DB;

class ProductTable extends AbstractAction
{
    public function index()
    {
        $dt = new Datatables(new MySQL(DB::getSettings()));

        $dt->query(
            "
        SELECT
          id AS sel_id,
          id AS edit_id,
          id,
          relation,
          category,
          name,
          model,
          sku,
          url,
          location,
          ean,
          jan,
          mpn,
          upc,
          discount_price,
          price,
          stock_status,
          manufacturer,
          description,
          attributes,
          images,
          images_d,     
          NULLIF(date_added, 0) AS date_added ,
          NULLIF(date_modified, 0) AS date_modified,
          quantity,
          status,
          del
        FROM kvp_shop_product
        {$this->makeWhere(FilterDTO::makeFromRequest($this->request))}
        ");

        //dd($dt->generate()->getQuery()->sql);

        return $dt->generate()->toJson();
    }

    public function makeWhere(FilterDTO $filterDTO, $withTableFilter = false)
    : string
    {
        $result = '';

        $whereConditions = $this->date_filters($filterDTO);

        if ($filterDTO->category){
            $whereConditions[] = 'LOWER(`category`) LIKE "' . DB::connect()->getMysqli()->real_escape_string(strtolower($filterDTO->category)) . '%"';
        }

        if ($filterDTO->manufacturer){
            $whereConditions[] = 'LOWER(`manufacturer`) = "' . DB::connect()->getMysqli()->real_escape_string(strtolower($filterDTO->manufacturer)) . '"';
        }

        if ($withTableFilter && $filterDTO->search != ''){
            $search = DB::connect()->getMysqli()->real_escape_string($filterDTO->search);
            $whereConditions[] = "`id` LIKE '%{$search}%' OR `name` LIKE '%{$search}%' OR `model` LIKE '%{$search}%'";
        }

        if (!empty($whereConditions)){
            $result = 'WHERE (' . implode(') AND (', $whereConditions) . ')';
        }

        return $result;
    }

    private function date_filters(FilterDTO $filterDTO)
    : array
    {
        $whereConditions = [];

        if ($filterDTO->lastUpdateFilter > 0){
            switch ($filterDTO->lastUpdateFilter){
                // Давно не обновлялись
                case 1:
                    $whereConditions[] = 'date_modified < NOW() - INTERVAL 14 DAY';
                    break;
                // Обновлялись сегодня
                case 2:
                    $whereConditions[] = 'DATE(`date_modified`) = DATE(NOW())';
                    break;
                /*case 3:
                    break;*/
            }
        }

        function makeWhereForDates ($field, $type){
            switch ($type){
                // За сегодня
                case 1:
                    return "DATE(`$field`) = DATE(NOW())";
                // За вчера
                case 2:
                    return "DATE(`$field`) = DATE(NOW() - INTERVAL 1 DAY)";
                // За текущий месяц
                case 3:
                    return "DATE(`$field`) > DATE(NOW() - INTERVAL 1 MONTH)";
                // За текущий год
                case 4:
                    return "YEAR(`$field`) = YEAR(NOW())";
            }

            return [];
        }

        if ($filterDTO->dateAddedFilter > 0){
            if (($where = makeWhereForDates('date_added', $filterDTO->dateAddedFilter)) != null){
                $whereConditions[] = $where;
            }
        }

        if ($filterDTO->dateModifiedFilter > 0){
            if (($where = makeWhereForDates('date_modified', $filterDTO->dateModifiedFilter)) != null){
                $whereConditions[] = $where;
            }
        }

        return $whereConditions;
    }

    public function updateActive()
    {
        $query = DB::connect()->query("UPDATE ?f SET status=?i WHERE id IN (?ai)",
            'kvp_shop_product',
            $this->request->get('action', 0),
            $this->request->get('items', []));

        return $query;
    }
}
