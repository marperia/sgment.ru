<?php

namespace System\Actions;

use System\Classes\AbstractAction;
use System\Classes\DB;
use System\Classes\Template;

class TableSettings extends AbstractAction
{
    public function form()
    : Template
    {
        $settings = DB::getSettings();

        $query = DB::connect()->query("
            SELECT COLUMN_NAME
            FROM INFORMATION_SCHEMA.COLUMNS
            WHERE table_schema = '?s' and table_name = 'kvp_shop_product'
            ORDER BY ORDINAL_POSITION", $settings['database']
        );
        $columns = [];
        while ($row = $query->fetchAssoc()) {
            $columns[] = [
                'name' => $row['COLUMN_NAME'],
                'hasImages' => $row['COLUMN_NAME'] == 'images'
            ];
        }


        return Template::make('pages.table_settings', compact('columns'));
    }
}
