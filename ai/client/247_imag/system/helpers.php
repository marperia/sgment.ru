<?php

function getBaseUrl()
: string
{
    return 'http' . (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
}

if (!function_exists('dd')) {
    /**
     * @return never
     */
    function dd()
    {
        echo '<pre>' . print_r(func_get_args(), true) . '</pre>';
        exit;
    }
}
