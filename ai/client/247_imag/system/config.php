<?php

return [
    'host' => getBaseUrl(),
    'cached_templates' => true,
    'databases' => [
        'default' => [
            'host'     => 'localhost',
            'port'     => '3306',
            'username' => 'dev_userdemo',
            'password' => 'dY0iO5dK',
            'database' => 'd1000610_demo',
        ],
    ],
];
