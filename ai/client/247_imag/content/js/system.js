class modal {
    constructor() {

    }

    open(action, params) {
        sys.loading(true);

        $('html').css('overflow', 'hidden');

        $('#window').click('click', (e) => {
            if(e.target.id == 'window'){
                this.close();
            }
        }).html('<div class="window_content"></div>').fadeIn(100);

        $('#window .window_content').load(sys.actionUrl(action, params), (data, status) => {
            sys.loading(false);

            $('#window').css('background-image', 'none');
            $('.window_content').fadeIn(100).one('click', '.window-close', (e) => {
                e.preventDefault();
                this.close();
            });

            $(document).one('keyup', (e) => {
                if (e.key == 'Escape'){
                    this.close();
                }
            });
        });
    }

    close() {
        $('#window').off('click').fadeOut(100).empty();
        $('html').css('overflow', 'auto');
    }
}

class config {
    constructor() {
        if (this._storageAvailable()){
            this.storage = window.localStorage;
            this.observer = [];
        }
        else {
            alert('Невозможно прочитать или сохранить настройки');
        }

        this._defaults = {
            settings: {
                page_length: 10,
                showing_html: 1,
                all_images: 0,
                images_size: 1,
                host: '',
                category_delimiter: ' > ',
            },
            state: {
                current_page: 0,
            },
            filters: {
                filter: '',
                search: '',
                last_upd_filter: 0,
                date_added_filter: 0,
                date_modified_filter: 0,
                category_filter: '',
                brand_filter: '',
            }
        };

        this.options = JSON.parse(JSON.stringify(this._defaults));

        this._read();
    }

    settings(name) {
        return this._getByName('settings', name);
    }

    state(name) {
        return this._getByName('state', name);
    }

    filter(name) {
        return this._getByName('filters', name);
    }

    getDefaults(){
        return this._defaults;
    }

    filterEnabled(){
        return !(JSON.stringify(this._defaults.filters) === JSON.stringify(this.options.filters));
    }

    imageSizeFromIndex(idx) {
        switch (parseInt(idx ?? this.options.settings.images_size)) {
            case 1:
                return {w: 50, h: 50};
            case 2:
            default:
                return {w: 100, h: 100};
            case 3:
                return {w: 150, h: 150};
            case 4:
                return {w: 200, h: 200};
        }
    }

    _read() {
        if (this.storage){
            let options = localStorage.getItem('tableConfig');

            if (options){
                options = JSON.parse(options);
                this.options = $.extend(true, this.options, options);
            }

            this._callObservers('read');
        }
    }

    _write() {
        localStorage.setItem('tableConfig', JSON.stringify(this.options));
        this._callObservers('change');
    }

    _getByName(section, name) {
        if (section){
            return this.options[section][name];
        }

        return null;
    }

    storeSettings(options) {
        this.options.settings = Object.assign(this.options.settings, options);
        this._write();
        this._callObservers('store_settings');
    }

    storeState(options) {
        this.options.state = Object.assign(this.options.state, options);
        this._write();
        this._callObservers('store_state');
    }

    storeFilters(options, value, callEvent) {
        if (typeof options == 'string'){
            this.options.filters[options] = value;
        }
        else {
            this.options.filters = Object.assign(this.options.filters, options);
        }

        this._write();

        if (typeof callEvent == 'undefined' || callEvent === true){
            this._callObservers('store_filters');
        }
    }

    /**
     * Добавляет наблюдателя
     *
     * @param {string} name
     * @param {string} event Доступные события: read, change, store_settings, store_state, store_filters.
     * @param {function} callback
     */
    addObserver(event, callback) {
        if (typeof callback == 'function'){
            this.observer.push({event, callback});
        }
    }

    /**
     * Вызывает наблюдатели
     *
     * @param {string} event
     * @private
     */
    _callObservers(event) {

        for (const item of this.observer) {
            if (item.event === event){
                try {
                    item.callback(this.options, event);
                }
                catch (e) {
                    console.exception(e);
                }
            }
        }
    }

    _storageAvailable() {
        let storage;

        try {
            storage = window['localStorage'];
            let x = '__storage_test__';

            storage.setItem(x, x);
            storage.removeItem(x);

            return true;
        }
        catch (e) {
            return e instanceof DOMException && (
                    // everything except Firefox
                    e.code === 22 ||
                    // Firefox
                    e.code === 1014 ||
                    // test name field too, because code might not be present
                    // everything except Firefox
                    e.name === 'QuotaExceededError' ||
                    // Firefox
                    e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
                // acknowledge QuotaExceededError only if there's something already stored
                (storage && storage.length !== 0);
        }
    }
}

class system {
    constructor() {
        this.modal = new modal();
        this.config = new config();
    }

    loading(action) {
        if (action){
            $('#loading').fadeIn(300);
        }
        else {
            $('#loading').fadeOut(300);
        }
    }

    contentUrl(url) {
        return this.buildUrl('content/' + url);
    }

    actionUrl(name, params) {
        return this.buildUrl('index.php', {action: name, ...params});
    }

    buildUrl(url, params) {
        if (params){
            let paramBuf = [];

            for (const paramKey in params) {
                paramBuf.push(`${paramKey}=${params[paramKey]}`);
            }

            params = '?' + paramBuf.join('&');
        }
        else {
            params = '';
        }

        return sys.config.settings('host') + '/' + url + params;
    }

    makeUrl(url, name) {
        return `<a href="${url}" target="_blank" rel="noreferrer">${name ?? 'Ссылка'}</a>`
    }

    cutString(text, length) {
        if (String(text).length > 0){
            return `<span title="${String(text).replace(/"/g, '\'')}">${this.escapeHtml(text).slice(0, length) + '[...]'}</span>`;
        }

        return '';
    }

    escapeHtml(text) {
        return String(text).replace(/"/g, '\'')
            .replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
    }

    stripHtml(html){
        let doc = new DOMParser().parseFromString(html, 'text/html');
        return doc.body.textContent || '';
    }

    sendData(url, type, data){
        return new Promise((resolve, reject) => {
            $.ajax({
                url: url,
                type: type,
                contentType: false,
                processData: false,
                data: data,
                success: resolve,
                error: reject,
            });
        });
    }
}


