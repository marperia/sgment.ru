class settingsPage {
    constructor() {
        const elements = document.querySelector('#table-config-form').elements;

        for (const element of elements) {
            const settings = sys.config.options.settings[element.dataset.item];

            if (settings){
                if (element.dataset.type == 'settings'){
                    this._setElementValue(element, settings);
                }
                else if (typeof settings[element.dataset.type] != 'undefined'){
                    this._setElementValue(element, settings[element.dataset.type]);
                }
            }
        }

        $('#table-config-form').submit((event) => this.saveSettings(event));
    }

    saveSettings(event) {
        event.preventDefault();

        let options = {};

        for (const element of event.target.elements) {
            if (element.classList.contains('cnf-option')){
                const name = (element.dataset.item) ? element.dataset.item : element.id;

                if (element.dataset.type == 'settings'){
                    options[name] = this._getElementValue(element);
                }
                else {
                    options[name] = Object.assign(options[name] ?? {},{[element.dataset.type]: this._getElementValue(element)});
                }
            }
        }

        sys.config.storeSettings(options);
        toastr.success('Настройки сохранены.');

        sys.modal.close();
    }

    _getElementValue(item) {
        if (item){
            switch (item.type) {
                case 'text':
                case 'select-one':
                    return item.value;

                case 'checkbox':
                    return +item.checked;
            }
        }
    }

    _setElementValue(item, value) {
        if (item){
            switch (item.type) {
                case 'text':
                case 'select-one':
                    item.value = value;
                    break;

                case 'checkbox':
                    item.checked = value == true;
                    break;
            }
        }
    }
}
