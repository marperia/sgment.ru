class productForm {
    constructor() {
        $('#product-form #add-image').click((e) => this.addImage(e));
        $('#product-form')
            .submit((event) => this.saveSettings(event))
            .on('click', '.remove_field', (e) => this.removeField(e.currentTarget));
    }

    addImage(e){
        e.preventDefault();
        let newImage = document.querySelector('#product-form .new-image').cloneNode(true);
        newImage.style.display = null;
        newImage.classList.remove('new-image');
        newImage = document.querySelector('#additional-images').appendChild(newImage);

        const input = newImage.querySelector('.input');
        input.removeAttribute('disabled');
        input.focus();
    }

    removeField(element) {
        element.parentNode.remove();
    }

    saveSettings(event) {
        event.preventDefault();
        sys.loading(true);

        const formData = new FormData(event.target);

        sys.sendData(sys.actionUrl('product_form_store'), 'post', formData)
            .then((response) => {
                if (response == 'OK'){
                    toastr.success('Успешно сохранено.');

                    pp.refreshTable();
                    sys.modal.close();
                }
                else {
                    alert(response);
                }
            }, (jqXHR, exception) => {
                toastr.error('Ошибка сохранения, проверьте форму и попробуйте еще раз.');
            })
            .finally(() => sys.loading(false));
    }

    _checkField(element){
        switch (element.dataset.filter) {
            default:
                return true;
        }
    }
}
