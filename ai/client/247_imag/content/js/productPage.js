class productPage {
    constructor() {
        this.columns = [
            {data: 'sel_id', width: '20px'},
            {title: '', data: 'edit_id'},
            {title: 'id', data: 'id'},
            {title: 'relation', data: 'relation'},
            {title: 'category', data: 'category', searchable: false, render: (data) => this.categoryRender(data)},
            {title: 'name', data: 'name', render: (data, type, row) => `<a class="row-edit open-product-form" data-item="${row.id}"><b>${data}</b></a>`},
            {title: 'model', data: 'model'},
            {title: 'sku', data: 'sku', searchable: false},
            {title: 'url', data: 'url', searchable: false, render: (data) => data ? sys.makeUrl(data) : data},
            {title: 'location', data: 'location', searchable: false, render: (data, type, row) => row.url ? sys.makeUrl(row.url, data) : data},
            {title: 'ean', data: 'ean', searchable: false},
            {title: 'jan', data: 'jan', searchable: false},
            {title: 'mpn', data: 'mpn', searchable: false},
            {title: 'upc', data: 'upc', searchable: false},
            {title: 'discount_price', data: 'discount_price', searchable: false},
            {title: 'price', data: 'price', searchable: false},
            {title: 'stock_status', data: 'stock_status', searchable: false},
            {title: 'manufacturer', data: 'manufacturer', searchable: false},
            {
                title: 'description',
                data: 'description',
                searchable: false,
                render: data => sys.cutString(sys.config.options.settings.showing_html ? data : sys.stripHtml(data), 30)
            },
            {title: 'attributes', data: 'attributes', searchable: false, render: (data, type) => this.attributeRender(data, type)},
            {title: 'images', data: 'images', searchable: false, render: (data, type) => this.imagesRender(data, type)},
            {title: 'images_d', data: 'images_d', searchable: false, render: (data, type) => this.imagesRender(data, type)},
            {title: 'date_added', data: 'date_added', searchable: false, class: 'context-menu', render: (data) => this.dateTimeRender(data)},
            {title: 'date_modified', data: 'date_modified', searchable: false, class: 'context-menu', render: (data) => this.dateTimeRender(data)},
            {title: 'quantity', data: 'quantity', searchable: false},
            {title: 'status', data: 'status', searchable: false, render: (data) => data == 1 ? 'Включен' : 'Отключен'},
            {title: 'del', data: 'del', searchable: false, render: (data) => data == 0 ? '+' : 'Удалён'},
        ];

        // Сброс при загрузке
        sys.config.storeFilters({category_filter: '', brand_filter: ''}, false);
        // ---

        this.imageSize = sys.config.imageSizeFromIndex();
        this.initTable();
        this.initSearch();
        this.initLastUpdFilter();
        this.showFilterText();

        sys.config.addObserver('store_settings', () => this.changedConfig());
        sys.config.addObserver('store_filters', () => this.showFilterText());

        $('#table-settings').click((e) => this.showSettings(e));
        $('#change-selected-rows-apply').click(() => this.changeSelectedRows());
        $('#reset-all-filters').click((e) => this.resetAllFilters(e));
        $('body').on('click', '.open-product-form', (e) => this.addEditForm(e.currentTarget));
    }

    initTable() {
        for (let i = 1; i < this.columns.length; i++) {
            const settings = sys.config.options.settings[this.columns[i].data];

            if (settings){
                this.columns[i].title = settings.name;
                this.columns[i].visible = settings.visible === 1;
                this.columns[i].orderable = settings.orderable === 1;
            }
        }

        this.dataTable = $('#price_table').DataTable({
            dom: 't<"#price_table_overlay.dataTables_overlay"<"pe-loader pe-fixed">>',
            language: {
                'url': sys.contentUrl('vendor/DataTables/i18n/ru.json'),
            },
            pageLength: parseInt(sys.config.options.settings.page_length),
            displayStart: sys.config.options.state.current_page * sys.config.options.settings.page_length,
            "lengthChange": false,
            'ajax': {
                type: 'POST',
                url: sys.actionUrl('product_table'),
                data: (data) => {
                    data.last_upd_filter = sys.config.filter('last_upd_filter');
                    data.date_added_filter = sys.config.filter('date_added_filter');
                    data.date_modified_filter = sys.config.filter('date_modified_filter');
                    data.category_filter = sys.config.filter('category_filter');
                    data.brand_filter = sys.config.filter('brand_filter');
                }
            },
            select: {
                style: 'multi',
                selector: '.row-selector',
                className: 'row-selected'
            },
            rowId: 'id',
            deferRender: true,
            "processing": true,
            "serverSide": true,
            "responsive": true,
            "destroy": true,
            order: [
                [2, 'asc']
            ],
            search: {
                search: sys.config.filter('search')
            },
            "columnDefs": [
                {
                    targets: 0,
                    searchable: false,
                    orderable: false,
                    render: (data, type) => {
                        if (type == 'display'){
                            return `<input type="checkbox" value="1" class="row-selector" data-item="${data}" data-type="row">`
                        }

                        return data;
                    },
                },
                {
                    targets: 1,
                    searchable: false,
                    orderable: false,
                    render: (data, type) => {
                        if (type == 'display'){
                            return `<i class="fas fa-edit row-edit open-product-form" title="Редактировать" data-item="${data}"></i>`
                        }

                        return data;
                    },
                },
                {
                    targets: '_all',
                    render: function (data, type) {
                        if (type === 'display' && !sys.config.options.settings.showing_html){
                            return sys.stripHtml(data);
                        }

                        return data;
                    }
                },
                {
                    targets: 15,
                    createdCell: function (td, cellData, rowData) {
                        if (rowData.quantity > 0){
                            td.classList.add('text-green');
                        }
                        else {
                            td.classList.add('text-red');
                        }
                    }
                },
                {
                    targets: 24,
                    createdCell: function (td, cellData, rowData) {
                        if (cellData == 1){
                            td.classList.add('text-green');
                        }
                        else {
                            td.classList.add('text-red');
                        }
                    }
                }
            ],
            columns: this.columns,
            initComplete: (settings, json) => {
                this.initPaginate();
                this.initSelectAll();
                this.initTableFilters();
            },
            drawCallback: () => {
                this.updateRecordsCount();
                this.updatePaginate();
            },
            /*headerCallback: (thead, data, start, end, display) => {
                 console.log($(this.dataTable.columns(19).header()));
            }*/
        });

        this.dataTable.on('processing.dt', function ( e, settings, processing ) {
            const overlay = $('#price_table_overlay');

            if (overlay){
                if (processing){
                    overlay.show();
                }
                else {
                    overlay.hide();
                }
            }
        });
    }

    recreateTable() {
        this.dataTable.destroy();
        this.dataTable = null;

        $('#table-paging').off();
        $('#price_table').empty().off();

        this.initTable();
    }

    /**
     * Обновление таблицы
     *
     * @param {(boolean|string)=} reset true = Полный сброс, false = мягкий сброс, page = обновить страницу.
     */
    refreshTable(reset = 'page') {
        if (reset === true){
            reset = 'full-reset';
            this.dataTable.rows().deselect();
        }
        else if (reset === false){
            reset = undefined;
        }

        this.dataTable.draw(reset);
    }

    showSettings(e) {
        e.preventDefault();
        sys.modal.open('table_settings');
    }

    addEditForm(element) {
        sys.modal.open('product_form', {id: element.dataset.item ?? 0});
    }

    initPaginate() {
        $('#table-paging').on('click', '.nav-item', (event) => {
            let page;

            if (event.target.classList.contains('point')){
                page = event.target.dataset.page;
            }
            else {
                page = parseInt(event.target.dataset.page);
            }

            this.dataTable.page(page).draw('page');
        });
    }

    initSearch() {
        document.querySelector('#searchform input[name="search"]').value = sys.config.filter('search');

        $.fn.select2.defaults.set('dropdownAutoWidth', true);
        $.fn.select2.defaults.set('allowClear', true);
        $.fn.select2.defaults.set('language', 'ru');
        $.fn.select2.defaults.set('placeholder', 'Выберите значение');

        $('#searchform').submit((event) => {
            event.preventDefault();

            sys.config.storeFilters('search', event.target.querySelector('input[name="search"]').value);

            this.dataTable.search(sys.config.filter('search'));
            this.refreshTable(false);
        });

        const selector = $('.select-filter[name="category"]').select2({
            //closeOnSelect: false,
            data: [],
            ajax: {
                url: sys.actionUrl('category_selector'),
                delay: 250,
                //cache: false,
                data: function (params) {
                    return Object.assign(params, {filtered: sys.config.filterEnabled()}, sys.config.options.filters);
                },
                processResults: (data) => {
                    data.results = data.results.map((item) => {
                        item.text = item.text.replaceAll('|', sys.config.options.settings.category_delimiter);

                        return item;
                    });

                    return data;
                }
            },

        }).on('select2:select', (e) => {
            sys.config.storeFilters('category_filter', e.target.value);

            this.refreshTable(false);
        }).on('select2:clear', () => {
            sys.config.storeFilters('category_filter', '');
            this.refreshTable(false);
        }).on('select2:open', (e) => {
            $('.select2-results__options .select2-results__option:not(.loading-results)').remove();
            setTimeout(() => {
                document.querySelector('.select2-search__field').focus();
            }, 10);
        });

        $('.select-filter[name="brand"]').select2({
            //closeOnSelect: false,
            ajax: {
                url: sys.actionUrl('brand_selector'),
                delay: 250,
               // cache: false,
                data: function (params) {
                    return Object.assign(params, {filtered: sys.config.filterEnabled()}, sys.config.options.filters);
                },
            }
        }).on('select2:select', (e) => {
            sys.config.storeFilters('brand_filter', e.target.value);
            this.refreshTable(false);
        }).on('select2:clear', () => {
            sys.config.storeFilters('brand_filter', '');
            this.refreshTable(false);
        }).on('select2:open', (e) => {
            $('.select2-results__options .select2-results__option:not(.loading-results)').remove();
            setTimeout(() => {
                document.querySelector('.select2-search__field').focus();
            }, 10);
        });
    }

    changeSelectedRows() {
        let selected = this.dataTable.rows({selected:true}).data().toArray();

        if (selected.length == 0){
            alert('Выберите записи');
            return;
        }

        const items = Array.from(selected, (item) => item.id);

        $.ajax({
            type: "POST",
            url: sys.actionUrl('update_active'),
            data: {items, action: document.querySelector('#change-status').value}
        }).done((response) => {
            if (response == 1){
                this.dataTable.rows().deselect();
                $('#price_table .row-selector-all').prop('checked', false);

                this.refreshTable();
            }
        });
    }

    updateRecordsCount() {
        let info = this.dataTable.page.info();

        document.querySelector('#total-records').innerText = info.recordsDisplay;
        document.querySelector('#records-info').innerText = `${info.start}-${info.end} из ${info.recordsDisplay}`;

        const search_count = document.querySelector('#search-count');
        if (sys.config.filter('search')){
            search_count.innerText = 'Результаты по запросу: ' + info.recordsDisplay;
        }
        else {
            search_count.innerText = '';
        }
    }

    changedConfig() {
        this.imageSize = sys.config.imageSizeFromIndex();

        this.recreateTable();
    }

    updatePaginate() {
        let info = this.dataTable.page.info();
        const navBlock = document.querySelector('#table-paging');

        sys.config.storeState({
            current_page: info.page
        });

        if (info.pages > 1){
            let buttons = [
                {'page': 'first', text: '« Первая', point: true},
                {'page': 'previous', text: '← Ранее', point: true},
                {'page': 'next', text: 'Далее →', point: true},
                {'page': 'last', text: 'Последняя »', point: true},
            ];

            let pagingButtons = [];

            if (info.page > 3){
                pagingButtons.push({'page': 0, text: 1});
                pagingButtons.push({'page': null, text: '<u>...</u>'});
            }

            const minPage = (info.page - 2 < 0) ? 0 : info.page - 2;
            const maxPage = (info.page + 3 > info.pages) ? info.pages : info.page + 3;

            for (let i = minPage; i < maxPage; i++) {
                pagingButtons.push({'page': i, text: i + 1});
            }

            if (info.pages - info.page > 4){
                pagingButtons.push({'page': null, text: '<u>...</u>'});
                pagingButtons.push({'page': info.pages - 1, text: info.pages});
            }

            buttons.splice(2, 0, ...pagingButtons);

            let paging = '';

            for (let i = 0; i < buttons.length; i++) {
                if (buttons[i].page == info.page){
                    paging += `<span class="nav-item-current">${buttons[i].text}</span>`;
                }
                else if (buttons[i].page != null){
                    paging += `<a data-page="${buttons[i].page}" class="nav-item ${buttons[i].point ? 'point' : 'page'}">${buttons[i].text}</a>`;
                }
                else {
                    paging += buttons[i].text;
                }
            }

            navBlock.innerHTML = paging;
            navBlock.style.display = null;
        }
        else {
            navBlock.style.display = 'none';
        }
    }

    initSelectAll() {
        this.dataTable.column(0).header().innerHTML = `<input type="checkbox" value="1" class="row-selector-all" data-type="all">`;
        this.dataTable.column(0).header().style.padding = 0;

        this.dataTable.on('click', '.row-selector-all', (e) => {
            if (e.target.checked){
                this.dataTable.rows().select();
                $('#price_table .row-selector').prop('checked', true);
            }
            else {
                this.dataTable.rows().deselect();
                $('#price_table .row-selector').prop('checked', false);
            }
        });
    }

    categoryRender(data) {
        if (data){
            return `<span title="${data.replaceAll('|', sys.config.options.settings.category_delimiter)}">${data.slice(data.lastIndexOf('|') + 1)}</span>`;
        }

        return data;
    }

    imagesRender(data, type) {
        if (type === 'display' && data.length > 0){
            const images = data.split(',');
            const count = sys.config.options.settings.all_images ? images.length : 1;
            let imgs = '';

            for (let i = 0; i < count; i++) {
                if (i == 0){
                    imgs += `<div><img alt="Нет изображения" src="${images[i]}" width="${this.imageSize.w}" style="min-width:${this.imageSize.w}px"></div>`;
                }
                else {
                    imgs += `<img src="${images[i]}" width="10">`;
                }
            }

            return `<div style="max-width:200px">${imgs}</div>`;
        }

        return data;
    }

    attributeRender(data, type) {
        if (data.length > 0){
            const result = data.matchAll(/<param-name>(.*?)<\/param-name>\s*<param-value>(.*?)<\/param-value>\s*/mg);
            let attributes = '';

            for (const item of result) {
                if (item.length == 3){
                    attributes += `${item[1]}: ${item[2]}<br>`.replace(/'/g, '"');
                }
            }

            return `<span title='${attributes.replaceAll('<br>', '\n')}'>${attributes.slice(0, 30)}[...]</span>`;
        }

        return data;
    }

    dateTimeRender(data) {
        if (!data){
            return '---';
        }

        const date = new Date(data);
        const cmpDate = new Date(data).setHours(0, 0, 0, 0);
        const now = new Date(new Date().setHours(0, 0, 0, 0));

        if (cmpDate == now.getTime()){
            return `Сегодня в ${String(date.getHours()).padStart(2, '0')}:${ String(date.getMinutes()).padStart(2, '0')}`;
        }

        if (cmpDate == now.setDate(now.getDate() - 1)){
            return `Вчера в ${String(date.getHours()).padStart(2, '0')}:${ String(date.getMinutes()).padStart(2, '0')}`;
        }

        return data;
    }

    initLastUpdFilter() {
        const controls = $('.last-upd-filter-control');

        controls.eq(sys.config.filter('last_upd_filter')).addClass('active');

        controls.click((e) => {
            e.preventDefault();

            const type = +e.target.dataset.type;

            if (type >= 0 && type <= 4){
                if (type != sys.config.filter('last_upd_filter')){
                    sys.config.storeFilters('last_upd_filter', type);

                    controls.removeClass('active');
                    controls.eq(type).addClass('active');

                    this.refreshTable(false);
                }
            }
        });
    }

    initTableFilters() {
        this.dataTable.columns(20).header()[0].appendChild(document.querySelector('.table-header-context-menu').cloneNode(true))
            .setAttribute('data-name', 'date_added');

        this.dataTable.columns(22).header()[0].appendChild(document.querySelector('.table-header-context-menu').cloneNode(true))
            .setAttribute('data-name', 'date_modified');

        this.dataTable.on('click', '.table-header-context-menu a', (e) => {
            const filterName = e.target.closest('div').dataset.name;

            if (filterName == 'date_added'){
                sys.config.storeFilters('date_added_filter', e.target.dataset.type);
            }
            else if(filterName == 'date_modified'){
                sys.config.storeFilters('date_modified_filter', e.target.dataset.type);
            }

            this.refreshTable(false);
        });
    }

    showFilterText() {
        if (!sys.config.filterEnabled()){
            document.querySelector('#reset-all-filters').style.display = 'none';
        }
        else {
            document.querySelector('#reset-all-filters').style.display = 'initial';
        }
    }

    resetAllFilters (e){
        e.preventDefault();
        sys.config.storeFilters(sys.config.getDefaults().filters);

        document.querySelector('#searchform input[name="search"]').value = '';
        this.dataTable.search(sys.config.filter('search'));

        $('.last-upd-filter-control').removeClass('active').eq(0).addClass('active');

        $('.select-filter[name="category"]').val('').trigger('change');
        $('.select-filter[name="brand"]').val('').trigger('change');

        this.refreshTable(false);
    }


}
