<?php

use WebPlace\Logger;

error_reporting(E_ALL | E_STRICT);
ini_set('error_reporting', (string) (E_ALL | E_STRICT));
date_default_timezone_set('Europe/Kiev');

define('ROOT_DIR', __DIR__);
define('SYSTEM_DIR', ROOT_DIR . DIRECTORY_SEPARATOR . 'system' . DIRECTORY_SEPARATOR);
define('STORAGE_DIR', ROOT_DIR . DIRECTORY_SEPARATOR . 'storage' . DIRECTORY_SEPARATOR);

define('CLASSES_DIR', SYSTEM_DIR . 'Classes' . DIRECTORY_SEPARATOR);
define('ACTIONS_DIR', SYSTEM_DIR . 'Actions' . DIRECTORY_SEPARATOR);
define('TEMPLATES_DIR', SYSTEM_DIR . 'Templates' . DIRECTORY_SEPARATOR);

define('LOGS_DIR', STORAGE_DIR . 'logs' . DIRECTORY_SEPARATOR);
define('CACHE_DIR', STORAGE_DIR . 'cache' . DIRECTORY_SEPARATOR);
define('CURRENT_TIME', time());

$loader = require ROOT_DIR . DIRECTORY_SEPARATOR . 'vendor/autoload.php';
Logger::setLogDirectory(LOGS_DIR);

$loader->addPsr4('System\\', SYSTEM_DIR);

require SYSTEM_DIR . 'helpers.php';
require SYSTEM_DIR . 'routes.php';

$system = System\Classes\System::getInstance();
$system->run();
